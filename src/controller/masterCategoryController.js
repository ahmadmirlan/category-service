import {MasterCategory} from '../models/masterCategoryModel';
import QueryBuilder from "../config/QueryBuilder";
import {pick} from 'lodash';
import {Category} from "../models/categoryModel";

/*
* POST
* Create Category
* */
export const createMasterCategory = async (req, res) => {
    const {name} = req.body;
    try {
        let masterCategory = new MasterCategory({name});
        masterCategory = await masterCategory.save();
        return res.send(masterCategory);
    } catch (e) {
        return res.status(400).sendError(e);
    }
};

/*
* GET
* Find all category
* */
export const findAllMasterCategory = async (req, res) => {
    try {
        let query = QueryBuilder(
            pick(req.body, [
                'filter',
                'pageNumber',
                'pageSize',
                'sortField',
                'sortOrder',
            ]),
        );
        const totalElements = await MasterCategory.countDocuments(query.filter);
        const masterCategories = await MasterCategory.find(query.filter)
            .limit(query.pageSize)
            .skip(query.pageSize * query.pageNumber)
            .sort([[query.sortField, query.sortOrder]]);
        return res.sendData(masterCategories, query.pageSize, totalElements, query.pageNumber);
    } catch (e) {
        return res.status(400).sendError('Failed load categories');
    }
};

/*
* Get
* Find category by id
* */
export const findMasterCategoryById = async (req, res) => {
    const {id} = req.params;

    if (!id) {
        return res.status(400).sendError('Please provide id');
    }

    try {
        const category = await MasterCategory.findOne({_id: id});

        if (!category) {
            return res.status(404).send('Category not found');
        }
        return res.send(category);
    } catch (e) {
        return res.status(500).sendError(e);
    }
};

/*
* PUT
* Update master category
* */
export const updateMasterCategory = async (req, res) => {
    const {id} = req.params;
    const updateDate = pick(req.body, ['name']);
    try {
        let findMasterCategory = await MasterCategory.findByIdAndUpdate(id, {...updateDate}, {new: true}).populate([
            {path: 'masterCategory', select: 'name'}
        ]);
        if (!findMasterCategory) {
            return res.status(404).sendMessage(`Master Category with id ${id} not found!`);
        }
        return res.send(findMasterCategory);
    } catch (e) {
        return res.status(400).sendError(e);
    }
};

/*
* Delete
* Remove master category
* */
export const removeMasterCategory = async (req, res) => {
    const {id} = req.params;
    try {
        const masterCategory = await MasterCategory.findById(id);
        if (!masterCategory) {
            return res.status(404).sendError('Please provided correct id');
        }
        const categoryCounts = await Category.countDocuments({masterCategory: masterCategory.id});
        if (categoryCounts && categoryCounts > 0) {
            return res.status(400).sendError('This master category used by some category');
        }

        await MasterCategory.findByIdAndDelete(id);
        return res.send({messages: `${masterCategory.name} removed`});
    } catch (e) {
        return res.status(500).sendError(e);
    }
};

/*
* Get
* Get all master category
* */
export const findAllMasterCategories = async (req, res) => {
    try {
        const categories = await MasterCategory.find();
        return res.send(categories);
    } catch (e) {
        return res.status(500).sendError('Cannot load all master categories');
    }
};
