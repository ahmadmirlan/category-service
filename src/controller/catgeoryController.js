import {Category} from '../models/categoryModel';
import {pick} from 'lodash';
import {MasterCategory} from "../models/masterCategoryModel";
import QueryBuilder from "../config/QueryBuilder";

/*
* POST
* Create new category
* */
export const createNewCategory = async (req, res) => {
    const {category, masterCategory} = req.body;

    try {
        let categoryData = new Category({category, masterCategory});
        const mcData = await MasterCategory.findById(masterCategory);
        if (!mcData) {
            return res.status(400).sendError('Invalid master category!');
        }

        categoryData = await categoryData.save();
        categoryData.masterCategory = mcData;
        return res.send(categoryData);
    } catch (e) {
        res.status(400).sendError(e);
    }
};

/*
* POST
* Find all category
* */
export const findAllCategories = async (req, res) => {
    try {
        let query = QueryBuilder(
            pick(req.body, [
                'filter',
                'pageNumber',
                'pageSize',
                'sortField',
                'sortOrder',
            ]),
        );
        const totalElements = await Category.countDocuments(query.filter);
        const categories = await Category.find(query.filter)
            .limit(query.pageSize)
            .skip(query.pageSize * query.pageNumber)
            .sort([[query.sortField, query.sortOrder]])
            .populate({path: 'masterCategory', select: 'name'});
        res.sendData(categories, query.pageSize, totalElements, query.pageNumber);
    } catch (e) {
        return res.status(400).sendError(e);
    }
};

/*
* PUT
* Update category
* */
export const updateCategory = async (req, res) => {
    const {id} = req.params;
    const categoryData = pick(req.body, ['status', 'masterCategory', 'category']);

    try {
        let findCategory = await Category.findByIdAndUpdate(id, {...categoryData}, {new: true}).populate({
            path: 'masterCategory',
            select: 'name'
        });

        if (!findCategory) {
            return res.status(404).sendMessage(`Category with id ${id} not found!`);
        }
        res.send(findCategory);
    } catch (e) {
        return res.status(400).sendError(e);
    }
};

/*
* DELETE
* Remove category
* */
export const deleteCategory = async (req, res) => {
    const {id} = req.params;
    try {
        const category = await Category.findById(id);
        if (!category) {
            return res.status(404).sendError('Category not found!');
        }
        await Category.findByIdAndDelete(id);
        return res.send({messages: `Category with id ${id} deleted!`});
    } catch (e) {
        return res.status(500).sendError('Cannot delete category');
    }
};

/*
* GET
* Find all active category
* */
export const findAllActiveCategory = async (req, res) => {
    try {
        const categories = await Category.find({status: 'ACTIVE'}).populate({path: 'masterCategory', select: 'name'});
        return res.send(categories);
    } catch (e) {
        return res.status(500).sendError('Failed load active category');
    }
};
