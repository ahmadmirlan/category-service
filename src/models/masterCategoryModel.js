import { Schema, model } from 'mongoose';
import toJson from '@meanie/mongoose-to-json';

const masterCategorySchema = new Schema({
    name: {
        type: String,
        required: true,
        minlength: 2,
        unique: true
    },
}, {
    timestamps: true,
    toJSON: {getters: true},
    toObject: {getters: true},
});
masterCategorySchema.plugin(toJson);
export const MasterCategory = model('MasterCategory', masterCategorySchema);
