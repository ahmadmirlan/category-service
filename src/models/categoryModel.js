import {Schema, model, ObjectId} from 'mongoose';
const toJson = require('@meanie/mongoose-to-json');

const categorySchema = new Schema({
    category: {
        type: String,
        required: true
    },
    status: {
      type: String,
      enum: ['ACTIVE', 'DISABLED'],
      default: 'ACTIVE'
    },
    masterCategory: {
        type: ObjectId,
        ref: 'MasterCategory',
        required: true
    }
}, {
    timestamps: true,
    toJSON: {getters: true},
    toObject: {getters: true},
});

categorySchema.plugin(toJson);

export const Category = model('Category', categorySchema);
