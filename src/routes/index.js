import * as MasterCategoryController from '../controller/masterCategoryController';
import * as CategoryController from '../controller/catgeoryController';
import {auth} from '../config/auth';

export const routes = (app) => {
    // Render home page
    app.get('/home', function (req, res) {
        return res.send({message: 'Welcome to BE Gateway'});
    });

    app.get('/version', (req, res) => {
        return res.send({
            version: 'v0.0.1'
        });
    });

    /*-----------------------Category Routes---------------------------------------*/
    app.post('/category', [auth], CategoryController.createNewCategory);
    app.put('/category/:id', [auth], CategoryController.updateCategory);
    app.post('/categories', [auth], CategoryController.findAllCategories);
    app.get('/categories/active', [auth], CategoryController.findAllActiveCategory);
    app.delete('/category/:id', [auth], CategoryController.deleteCategory);

    /*---------------------Master Category Routes-----------------------------*/
    app.post('/master/category', [auth], MasterCategoryController.createMasterCategory);
    app.post('/master/categories', [auth], MasterCategoryController.findAllMasterCategory);
    app.get('/master/category/:id', [auth], MasterCategoryController.findMasterCategoryById);
    app.get('/master/categories/all', [auth], MasterCategoryController.findAllMasterCategories);
    app.put('/master/category/:id', [auth], MasterCategoryController.updateMasterCategory);
    app.delete('/master/category/:id', [auth], MasterCategoryController.removeMasterCategory);
};
