const mongoose = require('mongoose');

export const connectDatabase = () => {
    const options = {
        useNewUrlParser: true,
        useUnifiedTopology: true
    };
    try {
        mongoose
            .connect(
                `mongodb://${process.env.DB_USERNAME}:${process.env.DB_PASSWORD}@cluster0-shard-00-00.m641b.mongodb.net:27017,cluster0-shard-00-01.m641b.mongodb.net:27017,cluster0-shard-00-02.m641b.mongodb.net:27017/${process.env.DB_NAME}?ssl=true&replicaSet=atlas-11x1hs-shard-0&authSource=admin&retryWrites=true&w=majority`,
                options
            )
            .catch(err => {
                console.error(err);
            });
    } catch (e) {
        console.error(e);
        return e;
    }
};
